#include <iostream>

class Animal
{
public:

   virtual void voice() = 0;

   virtual  ~Animal() {};
};

class Dog : virtual public Animal
{

public:

    void voice() override
    {
        std::cout << "Woof!\n";
    }

    virtual ~Dog() {};
};

class Cat : virtual public Animal
{

public:

    void voice() override
    {
        std::cout << "Meow!\n";
    }

    virtual ~Cat() {};
};


class Snake : virtual public Animal
{

public:

    void voice() override final
    {
        std::cout << "Sssss!\n";
    }

    virtual  ~Snake() {};
};


int main()
{
    Animal* arr[] = {new Dog(),new Cat(),new Snake()};
    
    for (int i = 0; i < 3; i++)
    {
        arr[i]->voice();
        delete arr[i];
    }

    return 0;
}

